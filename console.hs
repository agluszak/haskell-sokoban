{-# LANGUAGE OverloadedStrings #-}
import           Data.List
import           System.IO
import           Debug.Trace
import qualified Data.Text                     as T
import           Debug.Trace
worldRange = [-15 .. 15]
worldCoords = [ C x y | x <- worldRange, y <- worldRange ]

wall, ground, storage, box, invalid, player, startScreen, restartScreen
  :: Picture

wall = singleChar '#'
ground = singleChar '.'
storage = singleChar '*'
box = singleChar '$'
invalid = singleChar ' '
player = singleChar '@'
startScreen = (centeredLettering "SOKOBAN")
  & translated 0 2 (centeredLettering "Press Space to start")
restartScreen = (centeredLettering "YOU WON")
  & translated 0 (-2) (centeredLettering "Press R to restart")

nextLevelScreen :: Integer -> Picture
nextLevelScreen n =
  (centeredLettering $ "Cleared in " ++ (show n) ++ " moves")
    & translated 0 2 (centeredLettering "Press Space to continue")

data Tile = Wall | Ground | Storage | Box | Blank deriving (Eq)

drawTile :: Tile -> Picture
drawTile Wall    = wall
drawTile Ground  = ground
drawTile Storage = storage
drawTile Box     = box
drawTile Blank   = invalid

type Board = Coord -> Tile
data Maze = Maze Coord Board

level1 :: Board
level1 (C x y) | abs x > 4 || abs y > 4   = Blank
               | abs x == 4 || abs y == 4 = Wall
               | x == 2 && y <= 0         = Wall
               | x == 3 && y <= 0         = Storage
               | x >= -2 && y == 0        = Box
               | otherwise                = Ground

level2 :: Board
level2 (C x y) | abs x > 4 || abs y > 4   = Blank
               | abs x == 4 || abs y == 4 = Wall
               | x == 3 && y <= 0         = Storage
               | x == 2 && y <= 0         = Box
               | otherwise                = Ground

level3 :: Board
level3 (C x y) | abs x > 4 || abs y > 4   = Blank
               | abs x == 4 || abs y == 4 = Wall
               | abs x == 3 && abs y == 3 = Storage
               | abs x == 2 && abs y == 2 = Box
               | otherwise                = Ground

goodLevels :: [Maze]
goodLevels = [Maze (C 2 2) level3, Maze (C 2 2) level2, Maze (C 2 2) level1]

badLevel1 :: Board
badLevel1 (C x y) | abs x > 4 || abs y > 4   = Blank
                  | abs x == 4 || abs y == 4 = Wall
                  | x == 2                   = Wall
                  | x == 3 && y <= 0         = Storage
                  | x == 1 && y <= 0         = Box
                  | otherwise                = Ground

badLevel2 :: Board
badLevel2 (C x y) | abs x > 4 || abs y > 4       = Blank
                  | abs x == 4 || abs y == 4     = Wall
                  | abs ((abs x) + (abs y)) == 5 = Wall
                  | abs x == 3 && abs y == 3     = Storage
                  | abs x == 2 && abs y == 2     = Box
                  | otherwise                    = Ground

badLevel3 :: Board
badLevel3 (C x y) | abs x > 4 || abs y > 4   = Blank
                  | x == 2 && y == 4         = Blank
                  | abs x == 4 || abs y == 4 = Wall
                  | x == 2 && y <= 0         = Wall
                  | x == 3 && y <= 0         = Storage
                  | x >= -2 && y == 0        = Box
                  | otherwise                = Ground

badLevels :: [Maze]
badLevels =
  [Maze (C 2 2) badLevel1, Maze (C 2 2) badLevel2, Maze (C 2 2) badLevel3]

pictureOfBoard :: Board -> Picture
pictureOfBoard board =
  let tiles = map (\c -> atCoord c (drawTile $ board c)) worldCoords
  in  pictures tiles

data Direction = R | U | L | D deriving (Eq, Enum)
data Coord = C Integer Integer deriving (Eq, Show)

atCoord :: Coord -> Picture -> Picture
atCoord (C x y) pic = translated (fromIntegral x) (fromIntegral y) pic

adjacentCoord :: Direction -> Coord -> Coord
adjacentCoord R (C x y) = C (x + 1) y
adjacentCoord U (C x y) = C x (y + 1)
adjacentCoord L (C x y) = C (x - 1) y
adjacentCoord D (C x y) = C x (y - 1)

isPassable :: Tile -> Bool
isPassable Ground  = True
isPassable Storage = True
isPassable _       = False

radiansFromDirection :: Direction -> Double
radiansFromDirection U = 0
radiansFromDirection R = 3 * pi / 2
radiansFromDirection D = pi
radiansFromDirection L = pi / 2

rotatedPlayer :: Direction -> Picture
rotatedPlayer direction = player

data WorldState = WorldState {
    playerPosition :: Coord,
    playerDirection :: Direction,
    boxes :: [Coord],
    moves :: Integer,
    board :: Board
}

instance Eq WorldState where
  (WorldState pp1 pd1 box1 m1 b1) == (WorldState pp2 pd2 box2 m2 b2) =
    pp1 == pp2 && pd1 == pd2 && box1 == box2 && m1 == m2

directionFromEvent :: Event -> Maybe Direction
directionFromEvent RightMovement = Just R
directionFromEvent LeftMovement  = Just L
directionFromEvent DownMovement  = Just D
directionFromEvent UpMovement    = Just U
directionFromEvent _             = Nothing

handleMovement :: Direction -> WorldState -> WorldState
handleMovement direction state =
  let newPosition = adjacentCoord direction (playerPosition state)
      onlyRotated = WorldState (playerPosition state)
                               direction
                               (boxes state)
                               (moves state)
                               (board state)
      currentMaze = currentMazeState state
  in  case (currentMaze newPosition) of
        Wall -> onlyRotated
        Box ->
          let targetPosition = adjacentCoord direction newPosition
          in  if isPassable (currentMaze targetPosition)
                then
                  let updatedBoxes = map
                        (\c -> if c == newPosition then targetPosition else c)
                        (boxes state)
                  in  WorldState newPosition
                                 direction
                                 updatedBoxes
                                 (1 + moves state)
                                 (board state)
                else onlyRotated
        _ -> WorldState newPosition
                        direction
                        (boxes state)
                        (1 + moves state)
                        (board state)

handleEvent :: Event -> WorldState -> WorldState
handleEvent event state = if isWinning state
  then state
  else case (directionFromEvent event) of
    Just dir -> handleMovement dir state
    Nothing  -> state


data GameState world = StartScreen | Running world deriving Eq

data Activity world = Activity
        world
        (Event -> world -> world)
        (world -> Picture)

runActivity :: Activity s -> IO ()
runActivity (Activity initialState handle draw) =
  activityOf initialState handle draw

resettable :: Activity s -> Activity s
resettable (Activity state0 handle draw) = Activity state0 handle' draw
 where
  handle' Restart s = state0
  handle' e       s = handle e s

withStartScreen :: Activity s -> Activity (GameState s)
withStartScreen (Activity state0 handle draw) = Activity state0' handle' draw'
 where
  state0' = StartScreen

  handle' Continue StartScreen = Running state0
  handle' _        StartScreen = StartScreen
  handle' e        (Running s) = Running (handle e s)

  draw' StartScreen = startScreen
  draw' (Running s) = draw s

data WithLevels level state = WithLevels{
    levels:: [level],
    levelNo :: Integer,
    start :: (level->state),
    isWinningState :: (state->Bool),
    currentState :: Maybe state
    }

makeState :: [level] -> Integer -> (level -> state) -> Maybe state
makeState levels levelNo start = case (nth levels levelNo) of
  Nothing    -> Nothing
  Just level -> Just (start level)

withLevels
  :: [level]
  -> (level -> state)
  -> (state -> Bool)
  -> Activity state
  -> Activity (WithLevels level state)
withLevels levelsArg startArg isWinningArg (Activity state0 handle draw) =
  Activity state0' handle' draw' where
  state0' = WithLevels { levels         = levelsArg
                       , levelNo        = 0
                       , start          = startArg
                       , isWinningState = isWinningArg
                       , currentState   = makeState levelsArg 0 startArg
                       }
  handle' event wholeState@WithLevels { currentState = Just ss }
    | isWinningArg ss = case event of
      Continue ->
        let nextLevel = 1 + levelNo wholeState
        in  wholeState
              { levelNo      = nextLevel
              , currentState = makeState (levels wholeState) nextLevel startArg
              }
      otherwise -> wholeState
  handle' e wholeState@WithLevels { currentState = Nothing } = wholeState
  handle' e wholeState@WithLevels { currentState = Just ss } =
    wholeState { currentState = Just (handle e ss) }
  draw' WithLevels { currentState = Nothing } = restartScreen
  draw' WithLevels { currentState = Just ss } = draw ss

data WithUndo a = WithUndo a [a]

withUndo :: Eq a => Activity a -> Activity (WithUndo a)
withUndo (Activity state0 handle draw) = Activity state0' handle' draw' where
  state0' = WithUndo state0 []
  handle' Undo (WithUndo s stack) = case stack of
    s' : stack' -> WithUndo s' stack'
    []          -> WithUndo s []
  handle' e (WithUndo s stack) | s' == s   = WithUndo s stack
                               | otherwise = WithUndo (handle e s) (s : stack)
    where s' = handle e s
  draw' (WithUndo s _) = draw s

tileCoords :: Tile -> Board -> [Coord]
tileCoords tile board = [ c | c <- worldCoords, board c == tile ]

boxesCoords :: Board -> [Coord]
boxesCoords = tileCoords Box

storageCoords :: Board -> [Coord]
storageCoords = tileCoords Storage

removeBoxes :: Board -> Board
removeBoxes board = f . board
  where f = \tile -> if tile == Box then Ground else tile

addBoxes :: [Coord] -> Board -> Board
addBoxes boxesCoords board = \c -> if elem c boxesCoords then Box else board c

currentMazeState :: WorldState -> Board
currentMazeState state =
  let boxesRemoved = removeBoxes (board state)
  in  addBoxes (boxes state) boxesRemoved

isWinning :: WorldState -> Bool
isWinning state = allList (\c -> (board state) c == Storage) (boxes state)


draw :: WorldState -> Picture
draw state = if isWinning state
  then nextLevelScreen (moves state)
  else
    let drawPlayer =
          atCoord (playerPosition state) (rotatedPlayer $ playerDirection state)
    in  (translated 0 10 $ lettering $ ("Moves: " ++ (show $ moves state)))
        & drawPlayer
        & (pictureOfBoard $ currentMazeState state)

elemList :: Eq a => a -> [a] -> Bool
appendList :: [a] -> [a] -> [a]
listLength :: [a] -> Integer
filterList :: (a -> Bool) -> [a] -> [a]
nth :: [a] -> Integer -> Maybe a
mapList :: (a -> b) -> [a] -> [b]
andList :: [Bool] -> Bool
allList :: (a -> Bool) -> [a] -> Bool
reverseList :: [a] -> [a]
foldRightList :: (a -> b -> b) -> b -> [a] -> b
foldList :: (a -> b -> b) -> b -> [a] -> b

elemList elem = foldList (\head present -> head == elem || present) False
appendList list = foldRightList (\head acc -> head : acc) list
listLength = foldList (\head length -> length + 1) 0
filterList pred =
  foldRightList (\head acc -> if pred head then head : acc else acc) []
nth list n = snd $ foldList
  (\head acc ->
    if fst acc == n then (1 + fst acc, Just head) else (1 + fst acc, snd acc)
  )
  (0, Nothing)
  list
mapList f = foldRightList (\head acc -> (f head) : acc) []
andList = foldList (&&) True
allList f = foldList (\head acc -> (f head) && acc) True
reverseList = foldList (\head acc -> head : acc) []
foldRightList f acc list = foldList f acc (reverseList list)
foldList f acc []            = acc
foldList f acc (head : tail) = foldList f (f head acc) tail

isGraphClosedHelper
  :: Eq a => [a] -> [a] -> a -> (a -> [a]) -> (a -> Bool) -> Bool
isGraphClosedHelper seen queue current neighbours isOk = if not $ isOk current
  then False
  else case queue of
    [] -> True
    next : rest ->
      let newSeen  = current : seen
          newQueue = appendList rest
            $ filterList (\x -> not $ elemList x newSeen) (neighbours current)
      in  isGraphClosedHelper newSeen newQueue next neighbours isOk

isGraphClosed :: Eq a => a -> (a -> [a]) -> (a -> Bool) -> Bool
isGraphClosed = isGraphClosedHelper [] []
 where

reachable :: Eq a => a -> a -> (a -> [a]) -> Bool
reachable v initial neighbours =
  not $ isGraphClosed initial neighbours (\x -> x /= v)

allReachable :: Eq a => [a] -> a -> (a -> [a]) -> Bool
allReachable vs initial neighbours =
  allList (\v -> reachable v initial neighbours) vs

directions :: [Direction]
directions = [R, L, U, D]

tileNeighbours :: Board -> Coord -> [Coord]
tileNeighbours maze coord = if not $ isPassable (maze coord)
  then []
  else map (\d -> adjacentCoord d coord) directions

isClosed :: Maze -> Bool
isClosed (Maze start board) = isPassable (board start) && isGraphClosed
  start
  (tileNeighbours board)
  (\coord -> (board coord) /= Blank)

isSane :: Maze -> Bool
isSane (Maze start board) =
  let reachableBoxes = length $ filter
        (\c -> reachable c start (tileNeighbours board))
        (boxesCoords board)
      reachableStorages = length $ filter
        (\c -> reachable c start (tileNeighbours board))
        (storageCoords board)
  in  reachableBoxes <= reachableStorages

stateFromMaze :: Maze -> WithUndo WorldState
stateFromMaze (Maze coord board) =
  WithUndo (WorldState coord U (boxesCoords board) 0 (removeBoxes board)) []

initialState = WorldState (C 2 2) U [] 0 level1

isWinningWithUndo (WithUndo state stack) = isWinning state

main :: IO ()
main = do
  hSetBuffering stdin  NoBuffering
  hSetBuffering stdout NoBuffering
  runActivity
    $ resettable
    $ withStartScreen
    $ (withLevels goodLevels stateFromMaze isWinningWithUndo)
    $ withUndo
    $ Activity initialState handleEvent draw


clearScreen = "\ESC[2J\ESCc"

activityOf :: world -> (Event -> world -> world) -> (world -> Picture) -> IO ()
activityOf initialState handle draw = do
  contents <- getContents
  putStrLn $ screenOutput
    draw
    (scanl (handleMaybeEvent handle) initialState (handleInput contents))

handleMaybeEvent :: (Event -> state -> state) -> (state -> Maybe Event -> state)
handleMaybeEvent handle state Nothing      = state
handleMaybeEvent handle state (Just event) = handle event state

handleInput :: Input -> [Maybe Event]
handleInput ('\ESC' : rest) = handleInputEsc rest
handleInput ('r'    : rest) = (Just Restart) : (handleInput rest)
handleInput ('u'    : rest) = (Just Undo) : (handleInput rest)
handleInput (' '    : rest) = (Just Continue) : (handleInput rest)
handleInput (sth    : rest) = (Nothing) : (handleInput rest)

handleInputEsc ('[' : rest) = handleInputEsc2 rest
handleInputEsc (sth : rest) = (Nothing) : (handleInput rest)

handleInputEsc2 ('A' : rest) = (Just DownMovement) : (handleInput rest)
handleInputEsc2 ('B' : rest) = (Just UpMovement) : (handleInput rest)
handleInputEsc2 ('C' : rest) = (Just RightMovement) : (handleInput rest)
handleInputEsc2 ('D' : rest) = (Just LeftMovement) : (handleInput rest)
handleInputEsc2 (sth : rest) = (Nothing) : (handleInput rest)

type Input = String
data Event = Restart | Undo | Continue | DownMovement | UpMovement | RightMovement | LeftMovement
type DrawFun = Integer -> Integer -> Char
type Picture = DrawFun -> DrawFun

blank = id
(&) = (.)

translated :: Integer -> Integer -> Picture -> Picture
translated xDiff yDiff pic =
  let move f = \x y -> f (x + xDiff) (y + yDiff)
  in  \drawFun x y -> (pic $ move drawFun) (x - xDiff) (y - yDiff)

screenOutput :: (state -> Picture) -> [state] -> String
screenOutput draw (state : rest) =
  (drawFunToString (draw state $ empty)) ++ (screenOutput draw rest)

drawFunToString :: DrawFun -> String
drawFunToString fun =
  let linesFun y = [ fun x y | x <- worldRange ]
  in  clearScreen ++ unlines [ linesFun y | y <- worldRange ]

singleChar :: Char -> Picture
singleChar char =
  \background x y -> if x == 0 && y == 0 then char else background x y

lettering :: String -> Picture
lettering string = \background x y -> if y /= 0
  then background x y
  else case nth string x of
    Nothing   -> background x y
    Just char -> char

centeredLettering :: String -> Picture
centeredLettering string =
  let length = listLength string
  in  translated (-length `div` 2) 0 $ lettering string

empty :: DrawFun
empty = \x y -> ' '

pictures :: [Picture] -> Picture
pictures = foldr (&) blank
