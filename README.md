# SOKOBAN

Zastosowałem funkcyjną reprezentację rysowania 
```haskell
type DrawFun = Integer -> Integer -> Char
type Picture = DrawFun -> DrawFun
```

Poszczególne kafelki mają następujące oznaczenia:
```haskell
wall = singleChar '#'
ground = singleChar '.'
storage = singleChar '*'
box = singleChar '$'
invalid = singleChar ' '
player = singleChar '@' 
```

Kod nie wymagał wielu zmian względem poprzednich wersji, obecne są też wszystkie wcześniejsze funkcjonalności: cofanie, restartowanie, liczenie ruchów, przechodzenie między poziomami.

Okazało się, że w moim kodzie współrzędna Y rośnie w drugą stronę niż w CodeWorld, ale nie powinno to mieć żadnego znaczenia (plansze są odwrócone, ale oprócz tego identyczne).

Poprawnie obsługuję grę strzałkami (escape sequences).


